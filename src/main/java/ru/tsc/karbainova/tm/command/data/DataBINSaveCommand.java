package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBINSaveCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "data-save-bin";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save binary data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final Domain domain = getDomain();
        @NonNull final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NonNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NonNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
