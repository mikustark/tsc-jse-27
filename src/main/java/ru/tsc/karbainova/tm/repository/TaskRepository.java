package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Task findByIndex(@NonNull String userId, @NonNull int index) {
        final List<Task> entities = findAll(userId);
        return entities.get(index);
    }

//    @Override
//    public void clear() {
//        entities.clear();
//    }

    @NonNull
    public final Predicate<Task> predicateByName(@NonNull final String name) {
        return s -> name.equals(s.getName());
    }

    @Override
    public Task findByName(@NonNull String userId, @NonNull String name) {
        final List<Task> entities = findAll(userId);
        return entities.stream().filter(predicateByName(name)).findFirst().orElse(null);
    }

    @Override
    public Task removeByName(@NonNull String userId, @NonNull String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeByIndex(@NonNull String userId, @NonNull int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task taskUnbindById(@NonNull String userId, @NonNull String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(@NonNull String userId, @NonNull String projectId) {
        findAllTaskByProjectId(userId, projectId).forEach(o -> entities.remove(o.getId()));
    }

    @Override
    public Task bindTaskToProjectById(@NonNull String userId, @NonNull String projectId, @NonNull String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(@NonNull String userId, @NonNull String projectId) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && projectId.equals(o.getProjectId()))
                .collect(Collectors.toList());
    }
}
