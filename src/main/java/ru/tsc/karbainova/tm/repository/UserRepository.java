package ru.tsc.karbainova.tm.repository;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.IUserRepository;
import ru.tsc.karbainova.tm.model.User;

public class UserRepository implements IUserRepository {
    private List<User> users = new ArrayList<>();

    @NonNull
    public final Predicate<User> predicateByLogin(@NonNull final String login) {
        return s -> login.equals(s.getLogin());
    }

    public void addAll(List<User> entities) {
        if (entities == null) return;
        Map<String, User> newEntities = entities.stream()
                .collect(Collectors.toMap(User::getId, Function.identity(), (o1, o2) -> o1, LinkedHashMap::new));
        entities.addAll((Collection<? extends User>) newEntities);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @NonNull
    public final Predicate<User> predicateByEmail(@NonNull final String email) {
        return s -> email.equals(s.getEmail());
    }

    @NonNull
    public final Predicate<User> predicateById(@NonNull final String id) {
        return s -> id.equals(s.getId());
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users);
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(@NonNull final String id) {
        return users.stream()
                .filter(predicateById(id))
                .findFirst().orElse(null);
    }

    @Override
    public User findByLogin(@NonNull final String login) {
        return users.stream()
                .filter(predicateByLogin(login))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(@NonNull final String email) {
        return users.stream().filter(predicateByEmail(email)).findFirst().orElse(null);
    }

    @Override
    public User removeUser(@NonNull final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(@NonNull final String id) {
        final User user = findById(id);
        return removeUser(user);
    }

    @Override
    public User removeByLogin(@NonNull final String login) {
        final User user = findByLogin(login);
        return removeUser(user);
    }
}
